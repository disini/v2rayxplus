package core

import (
	"encoding/json"
	"gitlab.com/xiayesuifeng/v2rayxplus/conf"
	"io"
	"log"
	"os"
	"os/exec"
	"path"
	"strconv"
	"strings"
)

func CopyFile(src, target string) error {
	srcConfig, err := os.Open(src)
	if err != nil {
		return err
	}
	defer srcConfig.Close()

	targetConfig, err := os.OpenFile(target, os.O_CREATE|os.O_TRUNC|os.O_RDWR, 0644)
	if err != nil {
		return err
	}
	defer targetConfig.Close()

	_, err = io.Copy(targetConfig, srcConfig)
	if err != nil {
		return err
	}

	return nil
}

func GetConfigName(group string) (name, path string) {
	for i := 0; ; i++ {
		tmp := ""
		if i > 0 {
			tmp = strconv.FormatInt(int64(i), 10)
		}
		path = conf.V2rayConfigPath + "/" + group + "/config" + tmp + ".json"
		_, err := os.Stat(path)
		if err != nil && os.IsNotExist(err) {
			return "config" + tmp, path
		}
	}
}

func GetSu() string {
	su, err := exec.LookPath("kdesu")
	if err != nil {
		su, err = exec.LookPath("pkexec")
		if err != nil {
			return ""
		}
	}

	return su
}

func GetTargetConfig(config, group string) (*conf.V2rayConfig, error) {
	file, err := os.Open(conf.V2rayConfigPath + "/" + group + "/" + config + ".json")
	if err != nil {
		return nil, err
	}

	v2ray := conf.V2rayConfig{}
	if err = json.NewDecoder(file).Decode(&v2ray); err != nil {
		return nil, err
	}

	targetV2ray := conf.NewV2rayConfig()
	for i := 0; i < len(targetV2ray.OutboundConfigList); i++ {
		if targetV2ray.OutboundConfigList[i].Tag == "proxy" {
			for j := 0; j < len(v2ray.OutboundConfigList); j++ {
				if v2ray.OutboundConfigList[j].Tag == "proxy" {
					targetV2ray.OutboundConfigList[i] = v2ray.OutboundConfigList[j]
				}
			}
		}
	}

	return targetV2ray, nil
}

func CopyConfigToEtc(config, group string) bool {
	su := GetSu()
	if su == "" {
		return false
	}

	targetV2ray, err := GetTargetConfig(config, group)
	if err != nil {
		return false
	}

	tmpPath := path.Join(os.TempDir(), "v2rayTmp.json")
	if err := targetV2ray.Save(tmpPath); err != nil {
		return false
	}
	defer os.Remove(tmpPath)

	cmd := exec.Command(su, "cp", tmpPath, "/etc/v2ray/config.json")
	_, err = cmd.CombinedOutput()
	return err == nil
}

func StartV2rayXPlusSerive(config, group string) bool {
	if conf.HelperClient != nil {
		targetConfig, err := GetTargetConfig(config, group)
		if err != nil {
			return false
		}

		bytes, err := json.Marshal(&targetConfig)
		if err != nil {
			return false
		}

		return conf.HelperClient.StartService(bytes) == nil
	} else {
		log.Println("rollback to old implementation")
		CopyConfigToEtc(config, group)
		return StartService("v2rayxplus")
	}
}

func RestartV2rayXPlusSerive(config, group string) bool {
	if conf.HelperClient != nil {
		targetConfig, err := GetTargetConfig(config, group)
		if err != nil {
			return false
		}

		bytes, err := json.Marshal(&targetConfig)
		if err != nil {
			return false
		}

		return conf.HelperClient.RestartService(bytes) == nil
	} else {
		log.Println("rollback to old implementation")
		CopyConfigToEtc(config, group)
		return RestartService("v2rayxplus")
	}
}

func StatusV2rayXPlusSerive() (exited, enable bool) {
	return StatusService("v2rayxplus")
}

func StopV2rayXPlusSerive() bool {
	if conf.HelperClient != nil {
		return conf.HelperClient.StopService() == nil
	} else {
		log.Println("rollback to old implementation")
		return StopService("v2rayxplus")
	}
}

func StartService(service string) bool {
	if os.Getuid() != 0 {
		su := GetSu()
		if su == "" {
			return false
		}

		return exec.Command(su, "systemctl", "start", service).Run() == nil
	} else {
		return exec.Command("systemctl", "start", service).Run() == nil
	}
}

func RestartService(service string) bool {
	if os.Getuid() != 0 {
		su := GetSu()
		if su == "" {
			return false
		}

		return exec.Command(su, "systemctl", "restart", service).Run() == nil
	} else {
		return exec.Command("systemctl", "restart", service).Run() == nil
	}
}

func StopService(service string) bool {
	if os.Getuid() != 0 {
		su := GetSu()
		if su == "" {
			return false
		}

		return exec.Command(su, "systemctl", "stop", service).Run() == nil
	} else {
		return exec.Command("systemctl", "stop", service).Run() == nil
	}
}

func StatusService(service string) (exited, enable bool) {
	bytes, err := exec.Command("systemctl", "status", service).CombinedOutput()
	if err != nil {
		return exited, enable
	}

	lines := strings.Split(string(bytes), "\n")
	for _, line := range lines {
		line = strings.TrimSpace(line)
		if strings.HasPrefix(line, "Loaded:") {
			tmp := strings.Split(line, ";")
			if len(tmp) > 1 {
				if strings.Contains(tmp[1], "enable") {
					enable = true
				}
			}
		} else if strings.HasPrefix(line, "Active:") {
			exited = strings.Contains(line, "exited") || strings.Contains(line, "running")
		}
	}

	return exited, enable
}

func InitIpTables() {
	if exited, _ := StatusService("iptables"); !exited {
		if _, err := os.Stat("/etc/iptable/iptables.rules"); os.IsNotExist(err) {
			if _, err := os.Stat("/etc/iptable/empty.rules"); err == nil {
				CopyFile("/etc/iptable/empty.rules", "/etc/iptable/iptables.rules")
				StartService("iptables")
			}
		}
	}

	bytes, err := exec.Command("sysctl", "net.ipv4.ip_forward").CombinedOutput()
	result := strings.TrimSpace(string(bytes))
	if err == nil {
		if strings.HasSuffix(result, "0") {
			log.Println(string(bytes))
			exec.Command("sysctl", "net.ipv4.ip_forward=1").Run()
		}
	}
}
