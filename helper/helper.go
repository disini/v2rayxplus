package helper

import (
	"errors"
	"github.com/godbus/dbus/v5"
	"github.com/xiayesuifeng/go-polkit"
	"os"
)

var InterfaceName = "me.firerain.v2rayxplus"

type Helper struct {
	Authority        *polkit.Authority
	SystemdBusObject dbus.BusObject
	ExitChan         chan bool
}

func NewHelper() (*Helper, error) {
	authority, err := polkit.NewAuthority()
	if err != nil {
		return nil, err
	}

	return &Helper{Authority: authority, ExitChan: make(chan bool, 1)}, nil
}

func (h *Helper) StartService(bytes []byte) *dbus.Error {
	if err := h.checkAuthorization("startService"); err != nil {
		return err
	} else {
		if err := os.WriteFile("/etc/v2ray/config.json", bytes, 0644); err != nil {
			return dbus.MakeFailedError(err)
		}

		if err := h.SystemdBusObject.Call("org.freedesktop.systemd1.Manager.StartUnit", 0, "v2rayxplus.service", "replace").Err; err != nil {
			return dbus.MakeFailedError(err)
		}

		return nil
	}
}

func (h *Helper) RestartService(bytes []byte) *dbus.Error {
	if err := h.checkAuthorization("restartService"); err != nil {
		return err
	} else {
		if err := os.WriteFile("/etc/v2ray/config.json", bytes, 0644); err != nil {
			return dbus.MakeFailedError(err)
		}

		if err := h.SystemdBusObject.Call("org.freedesktop.systemd1.Manager.RestartUnit", 0, "v2rayxplus.service", "replace").Err; err != nil {
			return dbus.MakeFailedError(err)
		}

		return nil
	}
}

func (h *Helper) StopService() *dbus.Error {
	if err := h.checkAuthorization("stopService"); err != nil {
		return err
	} else {
		if err := h.SystemdBusObject.Call("org.freedesktop.systemd1.Manager.StopUnit", 0, "v2rayxplus.service", "replace").Err; err != nil {
			return dbus.MakeFailedError(err)
		}

		return nil
	}
}

func (h *Helper) StopHelper() *dbus.Error {
	if err := h.checkAuthorization("stopHelper"); err != nil {
		return err
	} else {
		h.ExitChan <- true

		return nil
	}
}

func (h *Helper) checkAuthorization(interfaceName string) *dbus.Error {
	authorization, err := h.Authority.CheckAuthorization(InterfaceName+"."+interfaceName, nil, polkit.CheckAuthorizationAllowUserInteraction, "")
	if err != nil {
		return dbus.MakeFailedError(err)
	}

	if authorization.IsAuthorized && !authorization.IsChallenge {
		return nil
	} else {
		return dbus.MakeFailedError(errors.New("unauthorized"))
	}
}
