package main

import (
	"github.com/godbus/dbus/v5"
	"github.com/godbus/dbus/v5/introspect"
	"gitlab.com/xiayesuifeng/v2rayxplus/helper"
	"log"
)

func main() {
	conn, err := dbus.SystemBus()
	if err != nil {
		log.Fatalln(err)
	}
	defer conn.Close()

	h, err := helper.NewHelper()
	if err != nil {
		log.Fatalln(err)
	}
	defer h.Authority.Close()

	h.SystemdBusObject = conn.Object("org.freedesktop.systemd1", "/org/freedesktop/systemd1")

	intro := introspect.NewIntrospectable(&introspect.Node{
		Interfaces: []introspect.Interface{{
			Name:    helper.InterfaceName,
			Methods: introspect.Methods(h),
		}},
	})

	conn.Export(h, "/me/firerain/v2rayxplus", helper.InterfaceName)
	conn.Export(intro, "/me/firerain/v2rayxplus", "org.freedesktop.DBus.Introspectable")

	if reply, err := conn.RequestName(helper.InterfaceName, dbus.NameFlagReplaceExisting); err != nil {
		log.Fatalln(err)
	} else if reply == dbus.RequestNameReplyAlreadyOwner {
		log.Fatalln("name already taken")
	}

	<-h.ExitChan
}
