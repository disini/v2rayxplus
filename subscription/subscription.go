package subscription

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"gitlab.com/xiayesuifeng/v2rayxplus/conf"
	"gitlab.com/xiayesuifeng/v2rayxplus/subscription/adapter"
	"io/ioutil"
	"net/http"
	"os"
	"path"
	"strings"
)

var adapterList map[string]Adapter

type Adapter interface {
	ConvertToOutbound(url string) (name string, outbound *conf.OutboundConfig, err error)
	RegistryAdapter() (name string)
}

func InitAdapter() {
	adapterList = make(map[string]Adapter)

	v2rayNAdapter := &adapter.V2rayNAdapter{}
	adapterList[v2rayNAdapter.RegistryAdapter()] = v2rayNAdapter
}

func GetAdapterNames() (names []string) {
	for name := range adapterList {
		names = append(names, name)
	}

	return
}

func UpdateSubscription(group, url, adapter string) (totalCount, availableCount int, err error) {
	configPath := path.Join(conf.V2rayConfigPath, group)
	_, err = os.Stat(configPath)
	if err != nil {
		if os.IsNotExist(err) {
			if err = os.Mkdir(configPath, 0755); err != nil {
				return
			}

			defer func() {
				if err != nil {
					os.Remove(configPath)
				}
			}()
		} else {
			return
		}
	}

	a, ok := adapterList[adapter]
	if !ok {
		return 0, 0, errors.New("not support this adapter")
	}

	data, err := getDataFromUrl(url)
	if err != nil {
		return
	}

	bytes, err := base64.URLEncoding.DecodeString(string(data))
	if err != nil {
		return
	}

	for _, url := range strings.Split(string(bytes), "\n") {
		totalCount++
		name, outbound, err := a.ConvertToOutbound(url)
		if err != nil {
			continue
		}

		outboundJson, err := json.Marshal(conf.V2rayConfig{OutboundConfigList: []*conf.OutboundConfig{outbound}})
		if err != nil {
			continue
		}

		err = ioutil.WriteFile(path.Join(configPath, name+".json"), outboundJson, 0644)
		if err != nil {
			continue
		}

		availableCount++
	}

	return
}

func getDataFromUrl(url string) ([]byte, error) {
	response, err := http.Get(url)
	if err != nil {
		return nil, err
	}

	if response.StatusCode == 200 {
		return ioutil.ReadAll(response.Body)
	} else {
		return nil, errors.New("HTTP request returns non-200 status code")
	}
}
