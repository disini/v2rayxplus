package adapter

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"gitlab.com/xiayesuifeng/v2rayxplus/conf"
	"strconv"
	"strings"
)

type V2rayNAdapter struct {
	Version string      `json:"v"`
	PS      string      `json:"ps"`
	Address string      `json:"add"`
	Port    interface{} `json:"port"`
	ID      string
	AlterId interface{} `json:"aid"`
	Network string      `json:"net"`
	Type    string
	Host    string
	Path    string
	Tls     string
}

func (v *V2rayNAdapter) ConvertToOutbound(url string) (name string, outbound *conf.OutboundConfig, err error) {
	if !strings.HasPrefix(url, "vmess://") {
		return "", nil, errors.New("only support vmess")
	}

	bytes, err := base64.StdEncoding.WithPadding(base64.NoPadding).DecodeString(strings.Replace(url, "vmess://", "", 1))
	if err != nil {
		if bytes, err = base64.StdEncoding.WithPadding(base64.StdPadding).DecodeString(strings.Replace(url, "vmess://", "", 1)); err != nil {
			return "", nil, err
		}
	}

	err = json.Unmarshal(bytes, v)
	if err != nil {
		return "", nil, err
	}

	port := uint16(0)
	switch v.Port.(type) {
	case string:
		if tmp, err := strconv.Atoi(v.Port.(string)); err == nil {
			port = uint16(tmp)
		}
	case float64:
		port = uint16(v.Port.(float64))
	}

	alterId := uint16(0)
	switch v.AlterId.(type) {
	case string:
		if tmp, err := strconv.Atoi(v.AlterId.(string)); err == nil {
			alterId = uint16(tmp)
		}
	case float64:
		alterId = uint16(v.AlterId.(float64))
	}

	outboundConfig := conf.VMessOutboundConfig{
		Receivers: []*conf.VMessOutboundTarget{
			{
				Address: v.Address,
				Port:    port,
				Users: []conf.VMessAccount{
					{
						ID:       v.ID,
						AlterIds: alterId,
					},
				},
			},
		},
	}

	settings, err := json.Marshal(&outboundConfig)
	if err != nil {
		return "", nil, err
	}

	streamSetting := &conf.StreamConfig{
		SocketSettings: &conf.SocketConfig{
			Mark: 255,
		},
	}

	var typeHeader *conf.HeaderConfig
	if v.Type != "none" {
		typeHeader = &conf.HeaderConfig{
			Type: v.Type,
		}
	}

	if v.Network == "tcp" && v.Type != "none" {
		streamSetting.TcpSettings = []byte(`{"type": "` + v.Type + `"}`)
	} else if v.Network == "kcp" {
		streamSetting.Network = "mkcp"
		streamSetting.KcpSettings = &conf.KcpConfig{
			Header: typeHeader,
		}

		if v.Path != "" {
			streamSetting.KcpSettings.Seed = v.Path
		}
	} else if v.Network == "ws" {
		streamSetting.Network = "ws"
		if v.Path != "" {
			streamSetting.WsSettings = &conf.WebSocketConfig{
				Path: v.Path,
			}
		}

		if v.Host != "" {
			streamSetting.WsSettings.Headers = map[string]string{"Host": v.Host}
		}
	} else if v.Network == "h2" {
		streamSetting.Network = "h2"
		if v.Path != "" {
			streamSetting.HttpSettings = &conf.HttpConfig{
				Path: v.Path,
			}
		}

		if v.Host != "" {
			streamSetting.HttpSettings.Host = strings.Split(v.Host, ",")
		}
	} else if v.Network == "quic" {
		streamSetting.Network = "quic"
		streamSetting.QuicSettings = &conf.QuicConfig{
			Header: typeHeader,
		}

		if v.Path != "" {
			streamSetting.QuicSettings.Key = v.Path
		}

		if v.Host != "" {
			streamSetting.QuicSettings.Security = v.Host
		}
	}

	if v.Tls == "tls" {
		streamSetting.Security = v.Tls
	}

	return v.PS, &conf.OutboundConfig{
		Protocol:      "vmess",
		Tag:           "proxy",
		Settings:      settings,
		StreamSetting: streamSetting,
	}, nil
}

func (v *V2rayNAdapter) RegistryAdapter() (name string) {
	return "V2RayN"
}
