package widgets

import (
	"github.com/fsnotify/fsnotify"
	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/widgets"
	"gitlab.com/xiayesuifeng/v2rayxplus/conf"
	core2 "gitlab.com/xiayesuifeng/v2rayxplus/core"
	"gitlab.com/xiayesuifeng/v2rayxplus/styles"
	"io/ioutil"
	"log"
	"os"
	"path"
	"path/filepath"
	"strings"
	"sync"
)

type ConfigList struct {
	widgets.QWidget

	vboxLayout *widgets.QVBoxLayout

	buttonGroup *widgets.QButtonGroup

	addButton *widgets.QPushButton

	ConfigName string

	groupName string

	watcher *fsnotify.Watcher

	firstConfigOnce sync.Once

	_ func() `constructor:"init"`

	_ func(name string) `signal:"addConfigListItem"`
	_ func(name string) `signal:"removeConfigListItem"`

	_ func(name string) `signal:"configChange"`
	_ func(name string) `signal:"editConfig"`
	_ func(name string) `signal:"removeConfig"`
	_ func(name string) `signal:"groupChange"`
}

func (ptr *ConfigList) init() {
	mainLayout := widgets.NewQVBoxLayout2(ptr)
	mainLayout.SetContentsMargins(18, 10, 18, 0)

	scrollArea := widgets.NewQScrollArea(ptr)
	scrollArea.SetHorizontalScrollBarPolicy(core.Qt__ScrollBarAlwaysOff)
	scrollArea.SetWidgetResizable(true)
	scrollArea.SetStyleSheet("background:transparent;border:0px;")
	scrollArea.VerticalScrollBar().SetStyleSheet(`QScrollBar{
  width: 8px;
  background: #DDD;
  border-radius: 4px;
}

QScrollBar::handle {
  background: #AAA;
  border-radius: 4px;
}

QScrollBar::handle:hover {
  background: #888;
}

QScrollBar::sub-line,QScrollBar::add-line {
  width: 0;
  height: 0; 
}`)

	listFrame := widgets.NewQFrame(scrollArea, 0)

	ptr.vboxLayout = widgets.NewQVBoxLayout2(scrollArea)
	ptr.vboxLayout.SetSpacing(0)
	ptr.vboxLayout.SetContentsMargins(0, 0, 0, 0)

	ptr.buttonGroup = widgets.NewQButtonGroup(ptr)

	ptr.addButton = widgets.NewQPushButton(ptr)
	ptr.addButton.SetFixedSize2(45, 45)
	ptr.addButton.SetStyleSheet(styles.GetStyleSheet(styles.AddButton))

	mainLayout.AddWidget(scrollArea, 1, 0)
	mainLayout.AddSpacing(20)
	mainLayout.AddWidget(ptr.addButton, 0, core.Qt__AlignHCenter)

	scrollArea.SetWidget(listFrame)

	listFrame.SetLayout(ptr.vboxLayout)
	ptr.SetLayout(mainLayout)

	ptr.initConnect()
	ptr.initWatcher()
}

func (ptr *ConfigList) initConnect() {
	ptr.buttonGroup.ConnectButtonClicked(func(button *widgets.QAbstractButton) {
		ptr.ConfigName = button.Text()
		ptr.ConfigChange(button.Text())
	})

	ptr.ConnectAddConfigListItem(ptr.addConfigListItem)

	ptr.ConnectRemoveConfigListItem(ptr.removeConfigListItem)

	ptr.ConnectGroupChange(ptr.scanConfList)

	ptr.addButton.ConnectClicked(func(checked bool) {
		v2ray := conf.V2rayConfig{OutboundConfigList: []*conf.OutboundConfig{
			{Tag: "proxy", Settings: []byte("{}"), StreamSetting: &conf.StreamConfig{SocketSettings: &conf.SocketConfig{Mark: 255}}},
		}}

		name, path := core2.GetConfigName(ptr.groupName)
		if err := v2ray.Save(path); err != nil {
			widgets.QMessageBox_Warning(ptr, "错误", err.Error(), widgets.QMessageBox__Ok, widgets.QMessageBox__Ok)
			return
		}

		ptr.EditConfig(name)
	})
}

func (ptr *ConfigList) scanConfList(group string) {
	log.Println(group)
	if ptr.groupName != "" {
		if err := ptr.watcher.Remove(path.Join(conf.V2rayConfigPath, ptr.groupName)); err != nil {
			log.Printf("cancel watching %s failed", err.Error())
		}
	}

	ptr.groupName = group

	if len(ptr.buttonGroup.Buttons()) > 0 {
		ptr.cleanConfList()
	}

	infos, err := ioutil.ReadDir(path.Join(conf.V2rayConfigPath, group))
	if err == nil {
		for _, info := range infos {
			if !info.IsDir() && strings.HasSuffix(info.Name(), ".json") {
				name := strings.Split(info.Name(), ".json")[0]
				ptr.addConfigListItem(name)
			}
		}
	}

	findSessionConfig := false

	ptr.firstConfigOnce.Do(func() {
		config := conf.Session.Config
		for _, button := range ptr.buttonGroup.Buttons() {
			if button.Text() == config {
				button.SetChecked(true)
				ptr.ConfigName = config
				findSessionConfig = true
				break
			}
		}
	})

	if !findSessionConfig && len(ptr.buttonGroup.Buttons()) > 0 {
		item := ptr.buttonGroup.Buttons()[0]
		item.SetChecked(true)
		ptr.ConfigName = item.Text()
	}

	if err = ptr.watcher.Add(path.Join(conf.V2rayConfigPath, group)); err != nil {
		log.Printf("watching %s failed", err.Error())
	}
}

func (ptr *ConfigList) addConfigListItem(name string) {
	tmp := NewConfigListItem2(name, ptr)
	tmp.ConnectEditConfig(ptr.EditConfig)
	tmp.ConnectRemoveConfig(ptr.RemoveConfig)
	tmp.ConnectRemoveConfig(func(name string) {
		os.Remove(conf.V2rayConfigPath + "/" + ptr.groupName + "/" + name + ".json")
	})
	ptr.vboxLayout.AddWidget(tmp, 0, core.Qt__AlignCenter)
	ptr.buttonGroup.AddButton(tmp, 0)
}

func (ptr *ConfigList) removeConfigListItem(name string) {
	for _, button := range ptr.buttonGroup.Buttons() {
		if button.Text() == name {
			ptr.buttonGroup.RemoveButton(button)
			ptr.vboxLayout.RemoveWidget(button)
			button.DestroyQAbstractButton()
			break
		}
	}
}

func (ptr *ConfigList) cleanConfList() {
	for _, button := range ptr.buttonGroup.Buttons() {
		ptr.buttonGroup.RemoveButton(button)
		ptr.vboxLayout.RemoveWidget(button)
		button.DestroyQWidget()
	}
}

func (ptr *ConfigList) initWatcher() {
	var err error
	ptr.watcher, err = fsnotify.NewWatcher()
	if err != nil {
		log.Printf("create watching failed: %s", err.Error())
	} else {
		go func() {
			for {
				select {
				case event, ok := <-ptr.watcher.Events:
					if !ok {
						return
					}
					if event.Op&fsnotify.Create == fsnotify.Create {
						filename := filepath.Base(event.Name)
						if strings.HasSuffix(filename, ".json") {
							ptr.AddConfigListItem(filename[0 : len(filename)-5])
						}
					} else if event.Op&fsnotify.Remove == fsnotify.Remove {
						filename := filepath.Base(event.Name)
						if strings.HasSuffix(filename, ".json") {
							ptr.RemoveConfigListItem(filename[0 : len(filename)-5])
						}
					}
				}
			}
		}()
	}
}
