package widgets

import (
	"github.com/therecipe/qt/core"
	"gitlab.com/xiayesuifeng/v2rayxplus/conf"
)

type DNSListModel struct {
	core.QAbstractListModel

	_ func() `constructor:"init"`

	_ func(row int)             `signal:"remove,auto"`
	_ func(item conf.DnsServer) `signal:"add,auto"`
	_ func(row int)             `signal:"copy,auto"`
	_ func(row int)             `signal:"upMove,auto"`
	_ func(row int)             `signal:"downMove,auto"`

	modelData []*conf.DnsServer
}

func (m *DNSListModel) init() {
	m.ConnectRowCount(m.rowCount)
	m.ConnectData(m.data)
	m.ConnectSetData(m.setData)

	m.ConnectFlags(func(index *core.QModelIndex) core.Qt__ItemFlag {
		return core.Qt__ItemIsSelectable | core.Qt__ItemIsEnabled | core.Qt__ItemIsEditable
	})
}

func (m *DNSListModel) setData(index *core.QModelIndex, value *core.QVariant, role int) bool {
	if role != int(core.Qt__EditRole) {
		return false
	}

	m.modelData[index.Row()].Address = value.ToString()
	m.DataChanged(index, index, []int{role})

	return true
}

func (m *DNSListModel) rowCount(*core.QModelIndex) int {
	return len(m.modelData)
}

func (m *DNSListModel) data(index *core.QModelIndex, role int) *core.QVariant {
	if role != int(core.Qt__DisplayRole) && role != int(core.Qt__EditRole) {
		return core.NewQVariant()
	}

	return core.NewQVariant1(m.modelData[index.Row()].Address)
}

func (m *DNSListModel) remove(row int) {
	if len(m.modelData) == 0 {
		return
	}
	m.BeginRemoveRows(core.NewQModelIndex(), row, row)
	if row == len(m.modelData)-1 {
		m.modelData = m.modelData[:row]
	} else {
		m.modelData = append(m.modelData[:row], m.modelData[row+1:]...)
	}
	m.EndRemoveRows()
}

func (m *DNSListModel) add(item conf.DnsServer) {
	m.BeginInsertRows(core.NewQModelIndex(), len(m.modelData), len(m.modelData))
	m.modelData = append(m.modelData, &item)
	m.EndInsertRows()
}

func (m *DNSListModel) copy(row int) {
	m.BeginInsertRows(core.NewQModelIndex(), len(m.modelData), len(m.modelData))
	m.modelData = append(m.modelData, m.modelData[row])
	m.EndInsertRows()
}

func (m *DNSListModel) upMove(row int) {
	if row == 0 {
		return
	}
	m.BeginMoveRows(core.NewQModelIndex(), row, row-1, core.NewQModelIndex(), 0)
	m.modelData[row], m.modelData[row-1] = m.modelData[row-1], m.modelData[row]
	m.EndMoveRows()
}

func (m *DNSListModel) downMove(row int) {
	if row == len(m.modelData)-1 {
		return
	}
	m.BeginInsertRows(core.NewQModelIndex(), len(m.modelData), len(m.modelData))
	m.modelData[row], m.modelData[row+1] = m.modelData[row+1], m.modelData[row]
	m.EndInsertRows()
}
