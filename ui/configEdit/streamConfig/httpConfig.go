package streamConfig

import (
	"encoding/json"
	"github.com/therecipe/qt/widgets"
	"gitlab.com/xiayesuifeng/v2rayxplus/conf"
	"log"
	"strings"
	"unicode"
)

type HttpConfig struct {
	*widgets.QWidget

	hostTextEdit    *widgets.QPlainTextEdit
	pathLineEdit    *widgets.QLineEdit
	methodComboBox  *widgets.QComboBox
	headersTextEdit *widgets.QPlainTextEdit
}

func NewHttpConfig(parent widgets.QWidget_ITF) *HttpConfig {
	widget := widgets.NewQWidget(parent, 0)

	httpConfig := &HttpConfig{QWidget: widget}
	httpConfig.init()

	return httpConfig
}

func (ptr *HttpConfig) init() {
	formLayout := widgets.NewQFormLayout(ptr)
	formLayout.SetContentsMargins(0, 0, 0, 0)

	ptr.hostTextEdit = widgets.NewQPlainTextEdit(ptr)
	ptr.pathLineEdit = widgets.NewQLineEdit(ptr)
	ptr.methodComboBox = widgets.NewQComboBox(ptr)
	ptr.headersTextEdit = widgets.NewQPlainTextEdit(ptr)

	ptr.hostTextEdit.SetPlaceholderText("多个域名用 ',' 分开,如 v2ray.com,v2fly.com")
	ptr.pathLineEdit.SetPlaceholderText("/")
	ptr.methodComboBox.AddItems([]string{"GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS", "HEAD"})
	ptr.methodComboBox.SetCurrentTextDefault("PUT")
	ptr.headersTextEdit.SetPlaceholderText(`{
  "key": ["val1","val2"]
}`)

	formLayout.AddRow3("host", ptr.hostTextEdit)
	formLayout.AddRow3("path", ptr.pathLineEdit)
	formLayout.AddRow3("method", ptr.methodComboBox)
	formLayout.AddRow3("headers", ptr.headersTextEdit)

	ptr.SetLayout(formLayout)
}

func (ptr *HttpConfig) saveConfig() *conf.HttpConfig {
	httpConfig := conf.HttpConfig{Path: ptr.pathLineEdit.Text()}

	httpConfig.Host = strings.FieldsFunc(strings.ReplaceAll(ptr.hostTextEdit.ToPlainText(), ",", " "), unicode.IsSpace)

	httpConfig.Method = ptr.methodComboBox.CurrentText()

	if headers := ptr.headersTextEdit.ToPlainText(); headers != "" {
		headersMap := map[string][]string{}
		if err := json.Unmarshal([]byte(headers), &headersMap); err != nil {
			log.Println("Failed to unmarshal headers, error:", err)
		} else {
			httpConfig.Headers = headersMap
		}
	}

	json, _ := json.Marshal(&httpConfig)
	if string(json) == "{}" {
		return nil
	}

	return &httpConfig
}
